# frozen_string_literal: true

require_relative 'spec_helper'

describe NominationProcessor do
  let(:team_members_file) { JSON.parse(File.read(File.join(__dir__, '/fixtures/team_members.json'))) }
  let(:review_submission) { JSON.parse(File.read(File.join(__dir__, '/fixtures/review_submission.json'))) }
  let(:slack_mock) { instance_double(PeopleGroup::Connectors::Slack, send_message: double(message: double(ts: '234567')), update_message: true, send_modal_message: true) }
  let(:slack_user) { double(user: double(id: 'something', profile: double(email: 'something@something.com'))) }
  let(:bamboo_mock) { instance_double(PeopleGroup::Connectors::Bamboo, add_bonus: true) }

  let(:nomination_object) do
    nomination = Nomination.create(
      nominee: 'Lien Van Den Steen',
      nominated_by: { slack_id: 'UTPD9QUJ0', slack_name: 'lvandensteen' },
      values: %w[collaboration dib],
      status: :manager_approved,
      reasoning: 'She is awesome',
      nominee_bamboo_id: 5,
      criteria: "Going above and beyond what is expected in regard to the team member's role description.",
      current_approver_bamboo_id: 22
    )
    nomination
  end

  let(:review_attachments) do
    [
      {
        text: 'Choose if you want to approve or reject this nomination',
        fallback: 'You are unable to approve or reject the nomination through Slack, you will have to do it manually.',
        callback_id: "uuid-#{nomination_object.id}",
        color: '#3AA3E3',
        attachment_type: 'default',
        actions: [
          {
            name: 'submission_approval',
            text: 'Approve',
            type: 'button',
            value: 'approve',
            "style": "primary"
          },
          {
            name: 'submission_approval',
            text: 'Reject',
            type: 'button',
            value: 'reject',
            "style": "danger"
          },
          {
            name: 'submission_approval',
            text: 'Update',
            type: 'button',
            value: 'update'
          }
        ]
      }
    ].to_json
  end

  let(:review_text) do
    nomination_message = <<~MSG
      <@lvandensteen> nominated Lien Van Den Steen for a discretionary bonus for the values: :collaboration-tanuki:, :diversity-tanuki:.
      They are nominating this team member because:

      ```She is awesome```

      According to the nominator, this meets the criteria because:
  
      ```Going above and beyond what is expected in regard to the team member's role description.```

      You can read more about the process in our handbook: https://about.gitlab.com/handbook/incentives/#nominator-bot-process
      Optionally, if you want to talk more in-depth about this nomination, please schedule a meeting with <@lvandensteen>.
    MSG
    nomination_message
  end

  before do
    allow(PeopleGroup::Connectors::Slack).to receive(:new).and_return(slack_mock)
    allow(slack_mock).to receive(:bamboo_email_lookup_with_fallback).with('nadia@flitglab.com').and_return({ 'user': { 'id': "1234", 'name': 'Nadia' } })

    allow(PeopleGroup::Connectors::Bamboo).to receive(:new).and_return(bamboo_mock)
    allow(bamboo_mock).to receive(:get_employee_details).with(5).and_return(team_members_file[4])
    allow(bamboo_mock).to receive(:fetch_manager).with(team_members_file[4]).and_return(team_members_file[5])
    allow(bamboo_mock).to receive(:slack_email_lookup_with_fallback).and_return(team_members_file[4])

    allow(slack_mock).to receive(:find_user_by_id).with('UTPD9QUJ0').and_return(slack_user)

    allow(ENV).to receive(:[]).with('LOCAL_TESTING').and_return(false)
  end

  describe '#process_review' do
    before do
      allow(bamboo_mock).to receive(:search_team_member).with('Lien Van Den Steen').and_return(team_members_file[4])
      allow(bamboo_mock).to receive(:fetch_second_level_manager).with(team_members_file[4]).and_return(team_members_file[6])
      allow(slack_mock).to receive(:bamboo_email_lookup_with_fallback).with('carol@flitglab.com').and_return({ 'user': { 'id': "6789", 'name': 'Carol' } })

      allow(Nomination).to receive(:find_by_callback_id).with("uuid-#{nomination_object.id}").and_return(nomination_object)
      allow(Nomination).to receive(:find_by_callback_id).with("uuid-3456-39038").and_return(nomination_object)

      nomination_object.update(status: :submitted)
      described_class.new.process_review(review_submission)
    end

    context 'when being rejected' do
      let(:review_submission) { JSON.parse(File.read(File.join(__dir__, '/fixtures/reject_payload.json'))) }

      it 'fetches the value and updates the manager rejection' do
        nomination_object.reload
        expect(nomination_object.status).to eq('manager_rejected')
      end

      it 'sends the rejection modal' do
        expect(slack_mock).to have_received(:send_modal_message)
      end

      it 'updates the review message' do
        status = <<~MSG
          You've submitted your review for the discretionary bonus nomination of Lien Van Den Steen.
          We've updated the nomination as rejected. Please reach out to <@lvandensteen> to give them some context why it was not approved.
          As a reminder, the following reasoning was used for the nomination:

          ```She is awesome```
          
          According to the nominator, this met the criteria because:
          
          ```Going above and beyond what is expected in regard to the team member's role description.```
        MSG
        expect(slack_mock).to have_received(:update_message).with(channel: 'DTMKLG5QS', timestamp: '1596011718.000600', text: status)
      end

      it 'sets the current approver id to nil' do
        nomination_object.reload
        expect(nomination_object.current_approver_bamboo_id).to be_nil
      end
    end

    it 'finds the nomination' do
      expect(Nomination).to have_received(:find_by_callback_id).with('uuid-3456-39038').once
    end

    it 'fetches the value and updates the manager approval' do
      nomination_object.reload
      expect(nomination_object.status).to eq('manager_approved')
    end

    it 'updates the review message' do
      status = <<~MSG
        You've submitted your review for the discretionary bonus nomination of Lien Van Den Steen.
        Thanks for approving this nomination. The nomination will move to the next step in the approval flow. You can find more information about this flow here: https://about.gitlab.com/handbook/incentives/#nominator-bot-process.
        As a reminder, the following reasoning was used for the nomination:

        ```She is awesome```

        This met the criteria because:
        
        ```Going above and beyond what is expected in regard to the team member's role description.```
      MSG
      expect(slack_mock).to have_received(:update_message).with(channel: 'DTMKLG5QS', timestamp: '1596011718.000600', text: status)
    end

    it 'fetches the second level manager' do
      expect(slack_mock).to have_received(:bamboo_email_lookup_with_fallback).with('carol@flitglab.com')
    end

    it 'triggers the next step' do
      review_indirect_manager_attachments = review_attachments.gsub!('submission_approval', 'second_level_review')
      text = review_text + 'FYI: This has already been approved by their direct manager. You are requested to review this nomination as the manager\'s manager.'
      expect(slack_mock).to have_received(:send_message).with(channel: nil, text: text, attachments: review_indirect_manager_attachments).once
    end

    describe 'when final step' do
      let(:review_submission) { JSON.parse(File.read(File.join(__dir__, '/fixtures/tr_review_submission.json'))) }

      it 'syncs the nomination to BambooHR' do
        expect(bamboo_mock).to have_received(:add_bonus).once
      end

      it 'informs the manager' do
        update_message = <<~MSG
          :tada: The discretionary bonus for Lien Van Den Steen by <@lvandensteen> has been approved and logged into BambooHR.
          As a reminder, they were nominated for the following values: :collaboration-tanuki:, :diversity-tanuki: and the reason was:

          ```She is awesome```
         
          This met the criteria because:
        
          ```Going above and beyond what is expected in regard to the team member's role description.```

          We will update the nominator about the status. Your next steps are:

          - Inform Lien Van Den Steen about their upcoming discretionary bonus
          - Share this with the GitLab team in the #team-member-updates channel
        MSG
        expect(slack_mock).to have_received(:send_message).with(channel: nil, text: update_message).once
      end

      it 'informs the nominator' do
        update_message = <<~MSG
          :tada: The discretionary bonus for Lien Van Den Steen that you submitted has been approved and logged into BambooHR.
          As a reminder, you nominated them for the following values: :collaboration-tanuki:, :diversity-tanuki: and the reason was:

          ```She is awesome```

          This met the criteria because:
        
          ```Going above and beyond what is expected in regard to the team member's role description.```

          Their manager will inform them and share this with the GitLab team in the #team-member-updates channel.

          Thank you for making sure we recognize our team members.
        MSG

        expect(slack_mock).to have_received(:send_message).with(channel: 'UTPD9QUJ0', text: update_message).once
      end
    end
  end
end
