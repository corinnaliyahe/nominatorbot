# frozen_string_literal: true

FactoryBot.define do
  factory :decision do
    made_by { { slack_name: 'John', slack_id: 'XTR567' } }
    status { 0 }
    nomination { association :nomination }
  end
end
