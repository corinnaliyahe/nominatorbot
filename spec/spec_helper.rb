# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'
require_relative '../app/slack_authorizer'
require_relative '../config/environment'
require 'rack/test'

SimpleCov.start

module RSpecMixin
  include Rack::Test::Methods
  def app
    Sinatra::Application
  end
end

# For RSpec 2.x and 3.x
RSpec.configure do |config|
  config.include RSpecMixin

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  Encoding.default_external = 'UTF-8'

  config.filter_run focus: true
  config.run_all_when_everything_filtered = true

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  # FactoryBot setup
  config.include FactoryBot::Syntax::Methods
  config.before(:suite) do
    FactoryBot.find_definitions
  end
end
