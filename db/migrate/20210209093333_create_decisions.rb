# frozen_string_literal: true

class CreateDecisions < ActiveRecord::Migration[6.0]
  def change
    create_table :decisions do |t|
      t.json :made_by, null: false
      t.integer :status, null: false
      t.belongs_to :nomination, foreign_key: true, null: false, type: :uuid

      t.timestamps null: false
    end
  end
end
