# frozen_string_literal: true

class UpdateNotNullTypes < ActiveRecord::Migration[6.0]
  def change
    change_column_null(:nominations, :nominee, false)
    change_column_null(:nominations, :nominated_by, false)
    change_column_null(:nominations, :values, false)
    change_column_null(:nominations, :reasoning, false)
    change_column_null(:nominations, :status, false)
  end
end
