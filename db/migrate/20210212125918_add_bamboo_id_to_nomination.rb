# frozen_string_literal: true

class AddBambooIdToNomination < ActiveRecord::Migration[6.0]
  def change
    add_column :nominations, :nominee_bamboo_id, :integer
    add_index :nominations, :nominee_bamboo_id
  end
end
