# frozen_string_literal: true

class AddRetriggerAtToNominations < ActiveRecord::Migration[6.0]
  def change
    add_column :nominations, :retriggered_at, :datetime
  end
end
