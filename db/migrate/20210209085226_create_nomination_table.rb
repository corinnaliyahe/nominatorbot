# frozen_string_literal: true

class CreateNominationTable < ActiveRecord::Migration[6.0]
  enable_extension 'pgcrypto' unless extension_enabled?('pgcrypto')

  def change
    create_table :nominations, id: :uuid do |t|
      t.string :nominee
      t.json :nominated_by
      t.string :values, array: true
      t.text :reasoning
      t.integer :status
      t.datetime :synced_to_bamboo_at
      t.string :slack_ts
      t.integer :spreadsheet_row_id

      t.timestamps null: false
    end
  end
end
