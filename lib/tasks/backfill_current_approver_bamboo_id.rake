# frozen_string_literal: true

# Temporarily script to fill in the nominee_bamboo_id so we can do easier lookups
# TODO: Remove this after it ran

namespace :backfill_current_approver_bamboo_id do
  task run: :environment do
    p "Starting the backfill."
    start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    bamboo_client = PeopleGroup::Connectors::Bamboo.new

    Nomination.where(current_approver_bamboo_id: nil, status: Nomination::TODO_BY_MANAGER_STATES).find_each do |nomination|
      team_member = bamboo_client.get_employee_details(nomination.nominee_bamboo_id)

      if nomination.status == "submitted"
        manager = bamboo_client.fetch_manager(team_member)
      else
        manager = bamboo_client.fetch_second_level_manager(team_member)
      end

      if team_member.nil?
        p "Can not find team manager for nomination #{nomination.id}"
        next
      end

      nomination.update!(current_approver_bamboo_id: manager['id'])
    end
    end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    p "Backfill done. It took #{end_time - start_time} seconds"
  end
end
