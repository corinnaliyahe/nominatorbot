# frozen_string_literal: true

require 'optparse'

namespace :offboarding do
  task :dry_run, [:bamboo_id] => :environment do |t, args|
    bamboo_id = args[:bamboo_id]
    return unless bamboo_id

    Nomination.where(current_approver_bamboo_id: bamboo_id).find_each do |nomination|
      p nomination
    end
  end

  task :run, [:bamboo_id] => :environment do |t, args|
    bamboo_id = args[:bamboo_id]
    return unless bamboo_id

    Nomination.where(current_approver_bamboo_id: bamboo_id).find_each do |nomination|
      Review::Manager.call(nomination: nomination) if nomination.submitted?
      Review::SecondLevel.call(nomination: nomination) if nomination.manager_approved?
    end
  end
end
