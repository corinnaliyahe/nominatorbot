# frozen_string_literal: true

class EventProcessor
  def run(event)
    event_type = event.dig('type')
    case event_type
    when 'app_mention'
      # Not covered right now
      # TODO
    when 'app_home_opened'
      HomeView::UserNominations.call(trigger_id: event['event_ts'], user_id: event['user'])
    end
  end
end
