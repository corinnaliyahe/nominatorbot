# frozen_string_literal: true

class CreateNomination < ApplicationService
  def initialize(submission:)
    @submission = submission
  end

  def call
    nomination = Nomination.create_from_slack!(@submission)
    thank_nominator(nomination)
    Review::Manager.call(nomination: nomination)
  end

  private

  def thank_nominator(nomination)
    nomination_message = <<~MSG
      Thanks for nominating #{nomination.nominee} for the values: #{nomination.values_in_emojis} because:

      ```#{nomination.reasoning}```

      You added this as how the criteria are met:
      
      ```#{nomination.criteria}```
    MSG

    slack_client.send_message(channel: nomination.nominator_slack_id, text: nomination_message)
  end
end
