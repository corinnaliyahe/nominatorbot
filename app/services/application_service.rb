# frozen_string_literal: true

class ApplicationService
  include Connectors

  def self.call(*args, &block)
    new(*args, &block).call
  end
end
