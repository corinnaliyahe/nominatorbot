# frozen_string_literal: true

# This class is responsible for asking the current (= the one who it is stuck with) reviewer
# to please review the nomination. For managers and second level, this will start a thread on
# the original message. For People Group (previously Total Rewards) this will send another message in the
# channel with a link to the original message. The reason to send another message is because we can't
# ping them personally as it is a group of reviewers.

module Review
  class Retrigger < ApplicationService
    include Helpers

    def initialize(nomination_id: nil, user: nil, nomination: nil)
      raise 'Needs to be called with either nomination_id or nomination' if nomination_id.nil? && nomination.nil?

      @nomination = Nomination.find(nomination_id.sub('reminder-', '')) if nomination_id
      @nomination = nomination if nomination
      @user = user
    end

    def call
      slack_client.send_message(channel: channel, text: message, thread_ts: use_thread?)
      @nomination.update!(retriggered_at: DateTime.now)
    end

    private

    def channel
      case @nomination.status
      when 'submitted'
        ENV['LOCAL_TESTING'] ? slack_client.bamboo_email_lookup_with_fallback(ENV['TEST_EMAIL']).dig('user', 'id') : find_manager_on_slack(@nomination)
      when 'manager_approved'
        ENV['LOCAL_TESTING'] ? slack_client.bamboo_email_lookup_with_fallback(ENV['TEST_EMAIL']).dig('user', 'id') : find_second_level_manager_on_slack(@nomination)
      when 'second_level_approved'
        ENV['PEOPLE_GROUP_CHANNEL']
      end
    end

    def message
      case @nomination.status
      when *%w[submitted manager_approved]
        ":waves: <@#{channel}> Friendly ping not to forget about this nomination."
      else
        permalink = slack_client.link_for_message(channel: channel, timestamp: @nomination.slack_ts)
        ":waves: Friendly ping not to forget about this nomination #{permalink.permalink}."
      end
    end

    def use_thread?
      case @nomination.status
      when *%w[submitted manager_approved]
        @nomination.slack_ts
      end
    end
  end
end
