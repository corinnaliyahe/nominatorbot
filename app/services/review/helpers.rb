# frozen_string_literal: true

module Review
  module Helpers
    include Connectors

    def message_attachments(nomination_id)
      [
        {
          text: 'Choose if you want to approve or reject this nomination',
          fallback: 'You are unable to approve or reject the nomination through Slack, you will have to do it manually.',
          callback_id: "uuid-#{nomination_id}",
          color: '#3AA3E3',
          attachment_type: 'default',
          actions: [
            {
              name: '`__REVIEW_STAGE_NAME__`',
              text: 'Approve',
              type: 'button',
              value: 'approve',
              "style": "primary"
            },
            {
              name: '`__REVIEW_STAGE_NAME__`',
              text: 'Reject',
              type: 'button',
              value: 'reject',
              "style": "danger"
            },
            {
              name: '`__REVIEW_STAGE_NAME__`',
              text: 'Update',
              type: 'button',
              value: 'update'
            }
          ]
        }
      ]
    end

    def find_manager_on_bamboo(nomination)
      team_member = bamboo_client.get_employee_details(nomination.nominee_bamboo_id)
      bamboo_client.fetch_manager(team_member)
    end

    def find_second_level_manager_on_bamboo(nomination)
      team_member = bamboo_client.get_employee_details(nomination.nominee_bamboo_id)
      bamboo_client.fetch_second_level_manager(team_member)
    end

    def find_manager_on_slack(nomination)
      return slack_client.bamboo_email_lookup_with_fallback(ENV['TEST_EMAIL']).dig('user', 'id') if ENV['LOCAL_TESTING']

      manager_on_bamboo = find_manager_on_bamboo(nomination)
      slack_user = slack_client.bamboo_email_lookup_with_fallback(manager_on_bamboo['workEmail'])
      slack_user.dig('user', 'id')
    end

    def find_second_level_manager_on_slack(nomination)
      return slack_client.bamboo_email_lookup_with_fallback(ENV['TEST_EMAIL']).dig('user', 'id') if ENV['LOCAL_TESTING']

      second_level_manager = find_second_level_manager_on_bamboo(nomination)
      slack_client.bamboo_email_lookup_with_fallback(second_level_manager['workEmail']).dig('user', 'id')
    end

    def basic_message(nomination)
      if nomination.criteria.nil?
        <<~MSG
          <@#{nomination.nominator_slack_handle}> nominated #{nomination.nominee} for a discretionary bonus for the values: #{nomination.values_in_emojis}.
          They gave the following reasoning for the nomination:
  
          ```#{nomination.reasoning}```
  
          You can read more about the process in our handbook: https://about.gitlab.com/handbook/incentives/#nominator-bot-process
          Optionally, if you want to talk more in-depth about this nomination, please schedule a meeting with <@#{nomination.nominator_slack_handle}>.
        MSG
      else
        <<~MSG
          <@#{nomination.nominator_slack_handle}> nominated #{nomination.nominee} for a discretionary bonus for the values: #{nomination.values_in_emojis}.
          They are nominating this team member because:
  
          ```#{nomination.reasoning}```
          
          According to the nominator, this meets the criteria because:
  
          ```#{nomination.criteria}```
  
          You can read more about the process in our handbook: https://about.gitlab.com/handbook/incentives/#nominator-bot-process
          Optionally, if you want to talk more in-depth about this nomination, please schedule a meeting with <@#{nomination.nominator_slack_handle}>.
        MSG
      end
    end

    def prompt_manager_for_review(nomination, second_level: false)
      attachments = message_attachments(nomination.id).to_json
      nomination_message = basic_message(nomination)

      if second_level
        manager_id_slack = find_second_level_manager_on_slack(nomination)
        bamboo_manager = find_second_level_manager_on_bamboo(nomination)
        nomination_message += 'FYI: This has already been approved by their direct manager. You are requested to review this nomination as the manager\'s manager.'
        attachments.gsub!('`__REVIEW_STAGE_NAME__`', 'second_level_review')
      else
        manager_id_slack = find_manager_on_slack(nomination)
        bamboo_manager = find_manager_on_bamboo(nomination)
      end

      attachments.gsub!('`__REVIEW_STAGE_NAME__`', 'submission_approval')

      response = slack_client.send_message(channel: manager_id_slack, text: nomination_message, attachments: attachments)
      nomination.update(slack_ts: response.message.ts, current_approver_bamboo_id: bamboo_manager['id'])
    end
  end
end
