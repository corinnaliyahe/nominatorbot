# frozen_string_literal: true

# This class is responsible for asking the people group DRI for a review of a nomination

module Review
  class PeopleGroup < ApplicationService
    include Helpers

    def initialize(nomination:)
      @nomination = nomination
    end

    def call
      nomination_message = basic_message(@nomination)
      nomination_message += "Your approval is the last step, manager and indirect manager have all approved the nomination."

      attachments = message_attachments(@nomination.id).to_json.gsub!('`__REVIEW_STAGE_NAME__`', 'total_rewards_approval')
      response = slack_client.send_message(channel: ENV['PEOPLE_GROUP_CHANNEL'], text: nomination_message, attachments: attachments)
      @nomination.update(slack_ts: response.message.ts, current_approver_bamboo_id: nil)
    end
  end
end
