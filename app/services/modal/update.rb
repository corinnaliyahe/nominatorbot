# frozen_string_literal: true

# The only responsibility of this service is to send the
# nomination update modal to wherever it was triggered.

module Modal
  class Update < ApplicationService
    def initialize(trigger_id:, nomination:, channel:, timestamp:)
      @trigger_id = trigger_id
      @nomination = nomination
      @channel = channel
      @timestamp = timestamp
    end

    def call
      view = @nomination.criteria.nil? ? view_json : view_with_criteria_json
      new_view = view
        .sub('`__NOMINEE__`', @nomination.nominator_slack_handle)
        .sub('`__REASONING__`', @nomination.reasoning)
        .sub('`__NOMINATION_ID__`', @nomination.id)
        .sub('`__TIMESTAMP__`', @timestamp)
        .sub('`__CHANNEL__`', @channel)
        .sub('`__CRITERIA__`', @nomination.criteria || '')

      slack_client.send_modal_message(trigger: @trigger_id, view: new_view)
    end

    private

    def view_json
      '{
        "title": {
          "type": "plain_text",
          "text": "Team Member Bonus"
        },
        "submit": {
          "type": "plain_text",
          "text": "Update"
        },
        "blocks": [
          {
            "type": "section",
            "text": {
              "type": "plain_text",
              "text": "Edit the nomination for `__NOMINEE__`.",
              "emoji": true
            }
          },
          {
            "block_id": "motivation",
            "type": "input",
            "element": {
              "action_id": "motivation",
              "type": "plain_text_input",
              "multiline": true,
              "initial_value": "`__REASONING__`"
            },
            "label": {
              "type": "plain_text",
              "text": "Motivation",
              "emoji": true
            }
          }
        ],
        "type": "modal",
        "private_metadata": "`__NOMINATION_ID__`",
        "callback_id": "channel-`__CHANNEL__`-ts-`__TIMESTAMP__`"
      }'
    end

    def view_with_criteria_json
      '{
        "title": {
          "type": "plain_text",
          "text": "Team Member Bonus"
        },
        "submit": {
          "type": "plain_text",
          "text": "Update"
        },
        "blocks": [
          {
            "type": "section",
            "text": {
              "type": "plain_text",
              "text": "Edit the nomination for `__NOMINEE__`.",
              "emoji": true
            }
          },
          {
            "block_id": "motivation",
            "type": "input",
            "element": {
              "action_id": "motivation",
              "type": "plain_text_input",
              "multiline": true,
              "initial_value": "`__REASONING__`"
            },
            "label": {
              "type": "plain_text",
              "text": "Why is this team member being nominated?",
              "emoji": true
            }
          },
          {
            "block_id": "criteria",
            "type": "input",
            "element": {
              "action_id": "criteria",
              "type": "plain_text_input",
              "multiline": true,
              "initial_value": "`__CRITERIA__`"
            },
            "label": {
              "type": "plain_text",
              "text": "How does this meet the criteria?",
              "emoji": true
            }
          }
        ],
        "type": "modal",
        "private_metadata": "`__NOMINATION_ID__`",
        "callback_id": "channel-`__CHANNEL__`-ts-`__TIMESTAMP__`"
      }'
    end
  end
end
