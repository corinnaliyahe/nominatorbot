# frozen_string_literal: true

# The only responsibility of this service is to send the
# nomination modal to wherever it was triggered.
# This way it can be triggered by Slack commands, actions
# and shortcuts.

module Modal
  class Nomination < ApplicationService
    attr_reader :trigger_id

    def initialize(trigger_id:)
      @trigger_id = trigger_id
    end

    def call
      slack_client.send_modal_message(trigger: @trigger_id, view: view_json)
    end

    private

    def view_json
      '{
        "title": {
          "type": "plain_text",
          "text": "Team Member Bonus"
        },
        "submit": {
          "type": "plain_text",
          "text": "Nominate"
        },
        "blocks": [
          {
            "block_id": "nominee",
            "type": "input",
            "element": {
              "type": "conversations_select",
              "action_id": "nominee",
              "placeholder": {
                "type": "plain_text",
                "text": "Team member\'s name"
              },
              "filter": {
                "include": [
                  "im"
                ],
                "exclude_bot_users": true
              }
            },
            "label": {
              "type": "plain_text",
              "text": "Team Member"
            }
          },
          {
            "type": "divider"
          },
          {
            "type": "section",
            "text": {
              "type": "mrkdwn",
              "text": "Read the handbook to submit a valid nomination."
            },
            "accessory": {
              "type": "button",
              "text": {
                "type": "plain_text",
                "text": "Open Handbook",
                "emoji": true
              },
              "value": "click_me_123",
              "url": "https://about.gitlab.com/handbook/incentives/#valid-and-invalid-criteria-for-discretionary-bonuses",
              "action_id": "button-action"
            }
          },
          {
            "block_id": "values",
            "type": "input",
            "element": {
              "action_id": "values",
              "type": "checkboxes",
              "options": [
                {
                  "text": {
                    "type": "plain_text",
                    "text": ":collaboration-tanuki: Collaboration",
                    "emoji": true
                  },
                  "value": "collaboration"
                },
                {
                  "text": {
                    "type": "plain_text",
                    "text": ":results-tanuki: Results",
                    "emoji": true
                  },
                  "value": "results"
                },
                {
                  "text": {
                    "type": "plain_text",
                    "text": ":efficiency-tanuki: Efficiency",
                    "emoji": true
                  },
                  "value": "efficiency"
                },
                {
                  "text": {
                    "type": "plain_text",
                    "text": ":diversity-tanuki: Diversity, Inclusion & Belonging",
                    "emoji": true
                  },
                  "value": "dib"
                },
                {
                  "text": {
                    "type": "plain_text",
                    "text": ":iteration-tanuki: Iteration",
                    "emoji": true
                  },
                  "value": "iteration"
                },
                {
                  "text": {
                    "type": "plain_text",
                    "text": ":transparency-tanuki: Transparency",
                    "emoji": true
                  },
                  "value": "transparency"
                }
              ]
            },
            "label": {
              "type": "plain_text",
              "text": "Values",
              "emoji": true
            }
          },
          {
            "block_id": "motivation",
            "type": "input",
            "element": {
              "action_id": "motivation",
              "type": "plain_text_input",
              "multiline": true
            },
            "label": {
              "type": "plain_text",
              "text": "Why are you nominating this team member?",
              "emoji": true
            }
          },
          {
            "block_id": "criteria",
            "type": "input",
            "element": {
              "action_id": "criteria",
              "type": "plain_text_input",
              "multiline": true
            },
            "label": {
              "type": "plain_text",
              "text": "How does this meet the criteria?",
              "emoji": true
            }
          }
        ],
        "type": "modal",
        "callback_id": "submit_nomination"
      }'
    end
  end
end
