# frozen_string_literal: true

module HomeView
  module Helpers
    private

    def parse_nominations(nominations, include_nominated_by: false)
      nominations.each_with_object([]) do |nomination, parsed_nominations|
        used_template = nomination_with_reminder_button if nomination.can_be_retriggered?
        used_template ||= include_nominated_by ? default_nomination : nomination_without_nominated_by

        parsed_nomination = used_template
                              .sub('`__NAME__`', nomination.nominee)
                              .sub('`__NOMINATOR__`', "<@#{nomination.nominator_slack_handle}>")
                              .sub('`__STATUS__`', parsed_status(nomination.status))
                              .sub('`__REASONING__`', nomination.reasoning)
                              .sub('`__VALUES__`', nomination.values_in_emojis)
                              .sub('`__ID__`', nomination.id)
                              .sub('`__CRITERIA__`', nomination.criteria || '')
        parsed_nomination.sub!("\\n Criteria: __", '') if nomination.criteria.nil?

        parsed_nominations << parsed_nomination
      end
    end

    def no_nominations
      '{
        "type": "section",
        "text": {
          "type": "plain_text",
          "text": "You haven\'t nominated anyone for a discretionary bonus yet.",
          "emoji": true
        }
      }'
    end

    def no_reviewed_nominations
      '{
        "type": "section",
        "text": {
          "type": "plain_text",
          "text": "You haven\'t reviewed any nominations for a discretionary bonus yet.",
          "emoji": true
        }
      }'
    end

    def default_nomination
      '{
        "type": "section",
        "text": {
          "type": "mrkdwn",
          "text": "Nominee: *`__NAME__`*\n Nominated by: *`__NOMINATOR__`*\n Status: `__STATUS__` \n Values: `__VALUES__` \n Why: _`__REASONING__`_ \n Criteria: _`__CRITERIA__`_"
        }
      },
      {
        "type": "divider"
      }'
    end

    def nomination_without_nominated_by
      '{
        "type": "section",
        "text": {
          "type": "mrkdwn",
          "text": "Nominee: *`__NAME__`* \n Status: `__STATUS__` \n Values: `__VALUES__` \n Why: _`__REASONING__`_ \n Criteria: _`__CRITERIA__`_"
        }
      },
      {
        "type": "divider"
      }'
    end

    def nomination_with_reminder_button
      '{
        "type": "section",
        "text": {
          "type": "mrkdwn",
          "text": "Nominee: *`__NAME__`*\n Status: `__STATUS__` \n Values: `__VALUES__` \n Why: _`__REASONING__`_ \n Criteria: _`__CRITERIA__`_"
        },
        "accessory": {
          "type": "button",
          "text": {
            "type": "plain_text",
            "text": "Remind Approver",
            "emoji": true
          },
          "value": "reminder-`__ID__`"
        }
      },
      {
        "type": "divider"
      }'
    end

    def home_view_template
      '{
        "type": "home",
        "blocks": [
          {
            "type": "header",
            "text": {
              "type": "plain_text",
              "text": "Welcome to the Nominator bot!"
            }
          },
          {
            "type": "actions",
            "elements": [
              {
                "type": "button",
                "text": {
                  "type": "plain_text",
                  "text": "My Nominations",
                  "emoji": true
                },
                "style": "primary",
                "value": "my_nominations"
              },
              {
                "type": "button",
                "text": {
                  "type": "plain_text",
                  "text": "Reviewed Nominations",
                  "emoji": true
                },
                "value": "my_reviews",
                "action_id": "my_reviews"
              },
              {
                "type": "button",
                "text": {
                  "type": "plain_text",
                  "text": "Nominate!",
                  "emoji": true
                },
                "value": "nominate_tm"
              }
            ]
          },
          {
            "type": "divider"
          },
          `__NOMINATIONS__`
        ]
      }'
    end

    def parsed_status(status)
      case status
      when *Nomination::REJECTED_STATES
        'Rejected - reach out to the nominee\'s manager if they haven\'t done so'
      when *Nomination::FULLY_APPROVED_STATES
        "Approved :tada:"
      when 'submitted'
        'Waiting for manager approval'
      when 'manager_approved'
        'Waiting for second level approval'
      when 'second_level_approved'
        'Waiting for People Group approval'
      else
        'Approved by manager, waiting for next approvals'
      end
    end
  end
end
