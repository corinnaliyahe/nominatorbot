# frozen_string_literal: true

# This class is responsible for fetching all the nominations that the
# user has submitted and showing them in the home view tab of the Slack app.

module HomeView
  class UserNominations < ApplicationService
    include Helpers

    def initialize(trigger_id:, user_id:)
      @trigger_id = trigger_id
      @user_id = user_id
    end

    def call
      nominations = Nomination.nominated_by(@user_id)

      if nominations.none?
        home_view = home_view_template.sub('`__NOMINATIONS__`', no_nominations)
      else
        parsed_nominations = parse_nominations(nominations)
        home_view = home_view_template.sub('`__NOMINATIONS__`', parsed_nominations.join(','))
      end

      slack_client.publish_view(user_id: @user_id, trigger: @trigger_id, view: home_view)
    rescue Slack::Web::Api::Errors::InvalidArguments => e
      Interface::Sentry.send_error("Can't show homeview for trigger #{@trigger_id} and user #{@user_id}")
      raise e
    end
  end
end
