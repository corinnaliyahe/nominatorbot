# frozen_string_literal: true

# The purpose of this class is to transform the Slack review
# payload in an object.

class SlackReview
  attr_reader :review

  ALLOWED_ACTION_NAMES = %w[submission_approval second_level_review pbp_approval total_rewards_approval].freeze

  def initialize(review)
    @review = review
  end

  def reviewer
    @review['user']
  end

  def status
    case action_name
    when 'submission_approval'
      value == 'reject' ? :manager_rejected : :manager_approved
    when 'second_level_review'
      value == 'reject' ? :second_level_rejected : :second_level_approved
    when 'total_rewards_approval'
      value == 'reject' ? :total_rewards_rejected : :total_rewards_approved
    end
  end

  def needs_update?
    decision_status == 'needs_update'
  end

  def decision_status
    return 'needs_update' if value === 'update'

    value == 'approve' ? :approved : :rejected
  end

  private

  def action_name
    @action_name ||= @review['actions'].find { |action| ALLOWED_ACTION_NAMES.include?(action['name']) }&.dig('name')
  end

  def value
    @value ||= @review['actions'].find { |action| action['name'] == action_name }&.dig('value')
  end
end
