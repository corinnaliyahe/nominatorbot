# frozen_string_literal: true

class Nomination < ActiveRecord::Base
  has_many :decisions, dependent: :destroy

  validates :nominee, presence: true
  validates :nominated_by, presence: true
  validates :values, presence: true
  validates :reasoning, presence: true
  validates :status, presence: true
  # Disabling the cop because this is a field that will disappear anyway
  validates :spreadsheet_row_id, uniqueness: true, allow_blank: true # rubocop:disable Rails/UniqueValidationWithoutIndex

  VALID_STATES = %w[submitted manager_approved manager_rejected second_level_approved second_level_rejected pbp_approved pbp_rejected total_rewards_approved total_rewards_rejected synced].freeze
  REJECTED_STATES = %w[manager_rejected second_level_rejected pbp_rejected total_rewards_rejected].freeze
  FULLY_APPROVED_STATES = %w[total_rewards_approved synced].freeze
  TODO_BY_MANAGER_STATES = %w[submitted manager_approved].freeze
  enum status: VALID_STATES

  VALUES_MAPPING = {
    'collaboration' => ':collaboration-tanuki:',
    'results' => ':results-tanuki:',
    'efficiency' => ':efficiency-tanuki:',
    'dib' => ':diversity-tanuki:',
    'iteration' => ':iteration-tanuki:',
    'transparency' => ':transparency-tanuki:'
  }.freeze

  def self.create_from_slack!(submission)
    submission = SlackSubmission.new(submission)
    create!(submission.as_nomination_params)
  end

  def self.find_by_callback_id(callback_id)
    if callback_id.starts_with?('uuid-')
      find(callback_id.sub('uuid-', ''))
    else
      find_by(spreadsheet_row_id: callback_id.to_i)
    end
  end

  def self.nominated_by(slack_id)
    where("nominated_by->>'slack_id' = ?", slack_id).order(created_at: :desc)
  end

  def self.reviewed_by(slack_id)
    # the .uniq is only needed for local development as we push all the nominations to
    # the same user
    joins(:decisions).where("made_by->>'slack_id' = ?", slack_id).order(created_at: :desc).uniq
  end

  def rejected?
    REJECTED_STATES.include?(status)
  end

  def approved?
    FULLY_APPROVED_STATES.include?(status)
  end

  def people_group_approved?
    total_rewards_approved?
  end

  def nominator_slack_handle
    nominated_by['slack_name']
  end

  def nominator_slack_id
    nominated_by['slack_id']
  end

  def values
    return nil unless super

    super.join(', ')
  end

  def values_in_emojis
    emoji_values = values
    VALUES_MAPPING.each { |k, v| emoji_values.sub!(k, v) }
    emoji_values
  end

  def reasoning
    super&.gsub("\"", '\'')
  end

  def criteria
    super&.gsub("\"", '\'')
  end

  def department
    team_member['department'] || 'Unknown'
  end

  def division
    team_member['division'] || 'Unknown'
  end

  def sync_to_bamboo
    return if ENV['LOCAL_TESTING'] # We don't want to add these to BambooHR while testing
    return unless people_group_approved?

    if team_member.nil?
      Interface::Sentry.send_error("Can't find team member on BambooHR for #{id}", self)
    else
      bamboo_client.add_bonus(team_member['id'], "#{reasoning} \n #{criteria}")
      update!(status: :synced, synced_to_bamboo_at: DateTime.now)
    end
  end

  def can_be_retriggered?(hours_ago = 24)
    !rejected? && !approved? && slack_ts.present? && !recently_retriggered?(hours_ago) && created_or_reviewed_at_least_x_hours_ago?(hours_ago)
  end

  private

  def team_member
    @team_member ||= bamboo_client.search_team_member(nominee)
  end

  def bamboo_client
    @bamboo_client ||= PeopleGroup::Connectors::Bamboo.new
  end

  def recently_retriggered?(hours_ago)
    retriggered_at && retriggered_at > Time.now - (3600 * hours_ago)
  end

  def created_or_reviewed_at_least_x_hours_ago?(hours_ago)
    return created_at < Time.now - (3600 * hours_ago) && decisions.last.created_at < Time.now - (3600 * hours_ago) if decisions.any?

    created_at < Time.now - (3600 * hours_ago)
  end
end
