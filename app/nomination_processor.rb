# frozen_string_literal: true

# The class is responsible for:
# - thanking the nominator
# - kicking off the approval process
# - processing the reviews by the approvers

class NominationProcessor
  include Connectors

  def process_review(review)
    callback_id = review.dig('callback_id')
    nomination = Nomination.find_by_callback_id(callback_id)
    Interface::Sentry.send_error("Can't find nomination for callback id #{callback_id}") if nomination.nil?

    slack_review = SlackReview.new(review)
    if slack_review.needs_update?
      Modal::Update.call(
        trigger_id: review['trigger_id'],
        nomination: nomination,
        channel: review.dig('channel', 'id'),
        timestamp: review.dig('message_ts')
      )
      return
    end

    nomination.update!(status: slack_review.status)
    nomination.decisions.create!(
      made_by: { slack_name: slack_review.reviewer['name'], slack_id: slack_review.reviewer['id'] },
      status: slack_review.decision_status
    )

    if nomination.rejected?
      nomination.update!(current_approver_bamboo_id: nil)

      Modal::Reject.call(
        trigger_id: review['trigger_id'],
        nomination: nomination
      )
    end

    update_review_message(review, nomination)
    call_next_step(nomination)
  end

  private

  def call_next_step(nomination)
    return Review::SecondLevel.call(nomination: nomination) if nomination.manager_approved?
    return Review::PeopleGroup.call(nomination: nomination) if nomination.second_level_approved?
    return unless nomination.people_group_approved?

    nomination.sync_to_bamboo
    inform_manager_and_nominator(nomination)
  end

  def inform_manager_and_nominator(nomination)
    inform_manager(nomination)
    inform_nominator(nomination)
  end

  def inform_manager(nomination)
    begin
      manager = find_manager_on_slack(nomination) unless ENV['LOCAL_TESTING']
    rescue StandardError => e
      Interface::Sentry.send_error("Can't find manager for nomination #{nomination.id}, error #{e}", nomination)
    end
    manager = slack_client.bamboo_email_lookup_with_fallback(ENV['TEST_EMAIL']).dig('user', 'id') if ENV['LOCAL_TESTING']

    if nomination.criteria.nil?
      update_message = <<~MSG
        :tada: The discretionary bonus for #{nomination.nominee} by <@#{nomination.nominator_slack_handle}> has been approved and logged into BambooHR.
        As a reminder, they were nominated for the following values: #{nomination.values_in_emojis} and the motivation was:
  
        ```#{nomination.reasoning}```
  
        We will update the nominator about the status. Your next steps are:
  
        - Inform #{nomination.nominee} about their upcoming discretionary bonus
        - Share this with the GitLab team in the #team-member-updates channel
      MSG
    else
      update_message = <<~MSG
        :tada: The discretionary bonus for #{nomination.nominee} by <@#{nomination.nominator_slack_handle}> has been approved and logged into BambooHR.
        As a reminder, they were nominated for the following values: #{nomination.values_in_emojis} and the reason was:
  
        ```#{nomination.reasoning}```

        This met the criteria because:
        
        ```#{nomination.criteria}```
  
        We will update the nominator about the status. Your next steps are:
  
        - Inform #{nomination.nominee} about their upcoming discretionary bonus
        - Share this with the GitLab team in the #team-member-updates channel
      MSG
    end

    slack_client.send_message(channel: manager, text: update_message)
  end

  def inform_nominator(nomination)
    if nomination.criteria.nil?
      update_message = <<~MSG
        :tada: The discretionary bonus for #{nomination.nominee} that you submitted has been approved and logged into BambooHR.
        As a reminder, you nominated them for the following values: #{nomination.values_in_emojis} and the motivation was:
  
        ```#{nomination.reasoning}```
  
        Their manager will inform them and share this with the GitLab team in the #team-member-updates channel.
  
        Thank you for making sure we recognize our team members.
      MSG
    else
      update_message = <<~MSG
        :tada: The discretionary bonus for #{nomination.nominee} that you submitted has been approved and logged into BambooHR.
        As a reminder, you nominated them for the following values: #{nomination.values_in_emojis} and the reason was:
  
        ```#{nomination.reasoning}```

        This met the criteria because:
        
        ```#{nomination.criteria}```
  
        Their manager will inform them and share this with the GitLab team in the #team-member-updates channel.
  
        Thank you for making sure we recognize our team members.
      MSG
    end

    slack_client.send_message(channel: nomination.nominator_slack_id, text: update_message)
  end

  def reject_update(nomination)
    <<~MSG
      We've updated the nomination as rejected. Please reach out to <@#{nomination.nominator_slack_handle}> to give them some context why it was not approved.
      As a reminder, the following reasoning was used for the nomination:

      ```#{nomination.reasoning}```
    MSG
  end

  def reject_update_with_criteria(nomination)
    <<~MSG
      We've updated the nomination as rejected. Please reach out to <@#{nomination.nominator_slack_handle}> to give them some context why it was not approved.
      As a reminder, the following reasoning was used for the nomination:

      ```#{nomination.reasoning}```
      
      According to the nominator, this met the criteria because:
    
      ```#{nomination.criteria}```
    MSG
  end

  def approve_update(nomination)
    <<~MSG
      Thanks for approving this nomination. The nomination will move to the next step in the approval flow. You can find more information about this flow here: https://about.gitlab.com/handbook/incentives/#nominator-bot-process.
      As a reminder, the following reasoning was used for the nomination:

      ```#{nomination.reasoning}```
    MSG
  end

  def approve_update_with_criteria(nomination)
    <<~MSG
      Thanks for approving this nomination. The nomination will move to the next step in the approval flow. You can find more information about this flow here: https://about.gitlab.com/handbook/incentives/#nominator-bot-process.
      As a reminder, the following reasoning was used for the nomination:

      ```#{nomination.reasoning}```

      This met the criteria because:
      
      ```#{nomination.criteria}```
    MSG
  end

  def update_review_message(review, nomination)
    text = "You've submitted your review for the discretionary bonus nomination of #{nomination.nominee}."

    case nomination.status
    when 'total_rewards_approved'
      status = "Thanks for approving this nomination. As this was the last step in the approval flow, the discretionary bonus is synced to BambooHR."
    when 'total_rewards_rejected'
      status = "Thanks for reviewing this nomination. Please reach out to the manager as to why the People Group can't approve this bonus."
    when *Nomination::REJECTED_STATES
      status = nomination.criteria.nil? ? reject_update(nomination) : reject_update_with_criteria(nomination)
    when *%w[manager_approved second_level_approved]
      status = nomination.criteria.nil? ? approve_update(nomination) : approve_update_with_criteria(nomination)
    end

    slack_client.update_message(channel: review.dig('channel', 'id'), timestamp: review.dig('message_ts'), text: text + "\n" + status)
  end

  def thank_reviewer(nomination, reviewer)
    if nomination.rejected?
      text = "We've updated the entry as rejected. Please reach out to <@#{nomination.nominator_slack_handle}> to give them some context why it was not approved"
    else
      text = "Thanks for approving this nomination. The nomination will move to the next step."
    end

    slack_client.send_message(channel: reviewer, text: text)
  end

  def find_manager_on_slack(nomination)
    team_member = bamboo_client.search_team_member(nomination.nominee)
    manager_on_bamboo = bamboo_client.fetch_manager(team_member)
    slack_user = slack_client.bamboo_email_lookup_with_fallback(manager_on_bamboo['workEmail'])
    slack_user.dig('user', 'id')
  end
end
