# frozen_string_literal: true

module Interface
  class Sentry
    def self.send_error(error, nomination = nil)
      return if ENV['LOCAL_TESTING']

      Raven.extra_context(nomination_id: nomination.id) if nomination
      Raven.capture_exception(error)
    end
  end
end
