# frozen_string_literal: true

module Connectors
  def slack_client
    @slack_client ||= PeopleGroup::Connectors::Slack.new
  end

  def bamboo_client
    @bamboo_client ||= PeopleGroup::Connectors::Bamboo.new
  end
end
